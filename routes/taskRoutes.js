//It contains all endpoints for our application
const express = require("express");
const router =  express.Router();
const taskController = require("../controllers/taskController.js");

//Section Routes
//It is responsible for defining or creating endpoints
//All the business logic is done in the controllers

router.get("/viewTasks", (req, res) => {
  taskController.getAllTasks().then(resultFromController => res.send(
    resultFromController ));
})

/* 1.  Create a route for getting a specific task. */
router.get("/specificTask/:id", (req, res) => {
  taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(
    resultFromController ));
})

router.post("/addNewTasks", (req, res) => {
    taskController.createTask(req.body).then(resultFromController => {
      res.send(resultFromController);
    })
})

router.put("/updateTask/:id", (req, res) => {
  taskController.updateTask(req.params.id, req.body).then(resultFromController => {
    res.send(resultFromController);
  })
})
/*6. Create a controller function for changing the status of a task to "complete". */
router.put("/:id/:status", (req, res) => {
  taskController.updateSpecificTask(req.params.id, req.params.status).then(resultFromController => {
    res.send(resultFromController);
  })
})

router.delete("/deleteTask/:id", (req, res) => {
  taskController.deleteTask(req.params.id).then(resultFromController => {
    res.send(resultFromController);
  })
})
module.exports = router;