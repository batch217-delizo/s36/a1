const e = require("express");
const Task = require("../models/task.js");

module.exports.getAllTasks = () =>{

    return Task.find({}).then(result => {
          return result;
    });

}

/* Create a controller function for retrieving a specific task. */
module.exports.getSpecificTask = (taskId) =>{
  console.log("data" + taskId)
  return Task.findById(taskId).then(result => {
        return result;
  });

}

module.exports.createTask = (requestBody) => {
    let newTask = new Task({
      name: requestBody.name
    })

    return newTask.save().then((task, error) => {
      if(error) {
        console.log(error);
        return false;
      } else {
        return task;
      }
    })
}    

module.exports.updateTask = (taskId,newContent) => {

  return Task.findById(taskId).then((result, error) => {
      if(error){
        console.log(error);
        return false;
      } 

      result.name = newContent.name;
      
      return result.save().then((updateTask, saveError) => {
          if(saveError) {
            console.log(error);
            return false;
          }else {
            return updateTask;
          }
      });
  });
}

/* Create a controller function for changing the status of a task to "complete". */

module.exports.updateSpecificTask = (taskId,taskStatus) => {

  return Task.findById(taskId).then((result, error) => {
      if(error){
        console.log(error);
        return false;
      } 

      result.status = taskStatus;
      
      return result.save().then((updateTask, saveError) => {
          if(saveError) {
            console.log(error);
            return false;
          }else {
            return updateTask;
          }
      });
  });
}

module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
        if(err){
          console.log(err);
          return false;
        } else {
          console.log(removedTask); 
          return removedTask; 
        }
    });
}