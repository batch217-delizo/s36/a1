//Setup dependencies/moduls
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoutes.js");

//Server Setup
const app = express();
const port = 3001;

//Middle ware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//DB Connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.qfnw1x1.mongodb.net/B217_to-do?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true
})


//Routes
app.use('/tasks', taskRoute);

app.listen(port, () => {
  console.log(`Server is running at port: ${port}`);
})